-- ASSIGN OF IIS APPPOOL\DefaultAppPool into the database

--/****** Object:  User [IIS APPPOOL\DefaultAppPool]    Script Date: 3/20/2017 11:42:56 PM ******/
--CREATE USER [IIS APPPOOL\DefaultAppPool] FOR LOGIN [IIS APPPOOL\DefaultAppPool] WITH DEFAULT_SCHEMA=[dbo]
--GO
--ALTER ROLE [db_datareader] ADD MEMBER [IIS APPPOOL\DefaultAppPool]
--GO
--ALTER ROLE [db_datawriter] ADD MEMBER [IIS APPPOOL\DefaultAppPool]
--GO



/****** Object:  Table [dbo].[Node]    Script Date: 3/20/2017 11:42:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Node](
	[Id] [int] NOT NULL,
	[Label] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Node] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NodeRelations]    Script Date: 3/20/2017 11:42:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NodeRelations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceNodeId] [int] NOT NULL,
	[DestinationNodeId] [int] NOT NULL,
 CONSTRAINT [PK_NodeRelations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[NodeRelations]  WITH CHECK ADD  CONSTRAINT [FK_NodeRelations_Node] FOREIGN KEY([SourceNodeId])
REFERENCES [dbo].[Node] ([Id])
GO
ALTER TABLE [dbo].[NodeRelations] CHECK CONSTRAINT [FK_NodeRelations_Node]
GO
