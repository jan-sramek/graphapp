﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceLayer
{
    public class DataManager : IDataManager
    {
        public void InsertNode(int id, string label, IList<int> adjacents)
        {
            using (var db = new Entities())
            {
                var node = db.Nodes.FirstOrDefault(n => n.Id == id);
                if (node == null)
                {
                    node = db.Nodes.Create();
                    node.Id = id;
                    db.Entry(node).State = System.Data.Entity.EntityState.Added;
                }

                if (node.Label != label)
                    node.Label = label;
       
                // Delete relations
                foreach (var nodeRelation in node.NodeRelations.Where(nodeRelation => !adjacents.Any(adj => adj == nodeRelation.DestinationNodeId)).ToList())
                {
                    db.NodeRelations.Remove(nodeRelation);           
                }

                // Add relations
                foreach (var adjacentId in adjacents)
                {
                    if (!node.NodeRelations.Any(nr => nr.DestinationNodeId == adjacentId))
                    {
                        var newNodeRelation = new NodeRelation();
                        db.Entry(newNodeRelation).State = System.Data.Entity.EntityState.Added;
                        newNodeRelation.DestinationNodeId = adjacentId;
                        newNodeRelation.SourceNodeId = node.Id;
                    }
                }

                db.SaveChanges();
            }
        }
    }
}
