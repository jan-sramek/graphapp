﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using QuickGraph;
using QuickGraph.Algorithms;

namespace ServiceLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GraphApp" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select GraphApp.svc or GraphApp.svc.cs at the Solution Explorer and start debugging.
    public class GraphApp : IGraphApp
    {
        public IList<int> CalculateShortestPath(int rootEdge, int targetEdge)
        {
            using (var db = new Entities())
            {
                var shortestPath = new List<int>();
                var nodes = db.Nodes.Select(n => n.Id).ToList();
                var edges = db.NodeRelations.ToList().Select(nr => new Edge<int>(nr.SourceNodeId, nr.DestinationNodeId)).ToList();
                
                AdjacencyGraph<int, Edge<int>> graph = new AdjacencyGraph<int, Edge<int>>(true);
                graph.AddVertexRange(nodes);
                graph.AddEdgeRange(edges);
                Func<Edge<int>, double> edgeCost = e => 1; // constant cost               

                // compute shortest paths
                var tryGetPaths = graph.ShortestPathsDijkstra(edgeCost, rootEdge);

                // query path for given vertices               
                IEnumerable<Edge<int>> path;
                if (tryGetPaths(targetEdge, out path))
                {
                    foreach (var edge in path)
                    {
                        var edgeId = db.NodeRelations.Single(n => n.SourceNodeId == edge.Source && n.DestinationNodeId == edge.Target).Id;
                        shortestPath.Add(edgeId);
                    }
                }

                return shortestPath;
            }          
        }
    }
}
