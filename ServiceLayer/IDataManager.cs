﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceLayer
{
    [ServiceContract]
    public interface IDataManager
    {
        [OperationContract]
        void InsertNode(int id, string label, IList<int> adjacents);
    }
}
