﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiceLayer
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFrontEndService" in both code and config file together.
	[ServiceContract]
	public interface IFrontEndService
	{
		[OperationContract]
		IList<NodeData> GetNodes();
	}

    [DataContract]
    public class NodeData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Label { get; set; }

        [DataMember]
        public IList<EdgeData> Edges { get; set; }        

        public static IList<NodeData> CreateNodeData(IList<Node> nodes)
        {
            var nodesData = new List<NodeData>();
            foreach (var node in nodes)
            {
                var edges = node.NodeRelations.Select(nr => new EdgeData { Id = nr.Id, Source = nr.SourceNodeId, Target = nr.DestinationNodeId }).ToList();

                var nodeData = new NodeData
                {
                    Id = node.Id,
                    Label = node.Label,
                    Edges = edges
                };

                nodesData.Add(nodeData);
            }

            return nodesData;
        }
    }

    [DataContract]
    public class EdgeData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Source { get; set; }

        [DataMember]
        public int Target { get; set; }
    }
}
