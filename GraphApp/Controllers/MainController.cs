﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GraphApp.Models;
using GraphApp.ViewModels;

namespace GraphApp.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Index()
        {
            var service = new FrontEndService.FrontEndServiceClient();
            var nodes = Node.CreateNodes(service.GetNodes());

            var viewModel = new MainViewModel();
            viewModel.nodes = nodes.Select(n => new NodeJson { data = new NodeDataJson { id = n.Id, Label = n.Label } }).ToArray();
            viewModel.edges = nodes.SelectMany(n => n.Edges).Select(e => new EdgeJson { data = new EdgeDataJson { id = e.Id, source = e.Source, target = e.Target } }).ToArray();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CalculateShortestPath(int rootEdge, int targetEdge)
        {
            var graphAppService = new GraphAppService.GraphAppClient();
            var shortestPath = graphAppService.CalculateShortestPath(rootEdge, targetEdge);
            return Json(shortestPath);
        }
    }
}