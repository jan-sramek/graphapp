﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Models
{
    public class Edge
    {
        public int Id { get; set; }

        public int Source { get; set; }

        public int Target { get; set; }
    }
}
