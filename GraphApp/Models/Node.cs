﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphApp.Models
{
    public class Node
    {
        public int Id { get; set; }

        public string Label { get; set; }

        public IList<Edge> Edges { get; set; }

        public static IList<Node> CreateNodes(List<FrontEndService.NodeData> nodesService)
        {
            var nodes = new List<Node>();
            foreach (var nodeService in nodesService)
            {
                var edges = nodeService.Edges.Select(nr => new Edge { Id = nr.Id, Source = nr.Source, Target = nr.Target }).ToList();

                var node = new Node
                {
                    Id = nodeService.Id,
                    Label = nodeService.Label,
                    Edges = edges
                };

                nodes.Add(node);
            }

            return nodes;
        }
    }
}
