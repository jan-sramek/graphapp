﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using GraphApp.Models;

namespace GraphApp.ViewModels
{
    public class MainViewModel
    {
        public NodeJson[] nodes { get; set; }
        public EdgeJson[] edges { get; set; }
    }

    public class NodeJson
    {
        public NodeDataJson data { get; set; }
    }

    public class NodeDataJson
    {
        public int id { get; set; }
        public string Label { get; set; }
    }

    public class EdgeJson
    {
        public EdgeDataJson data { get; set; }
    }

    public class EdgeDataJson
    {
        public int id { get; set; }
        public int source { get; set; }
        public int target { get; set; }
    }
}