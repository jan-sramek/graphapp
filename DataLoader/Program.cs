﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Configuration;


namespace DataLoader
{
    public class Program
    {
        static void Main(string[] args)
        {
            var folderPath = ConfigurationSettings.AppSettings["InputDataLocation"].ToString();
            XmlDataLoader.Run(folderPath);          
        }
    }
}
