﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader
{
    internal class NodeData
    {
        public int Id { get; set; }

        public string Label { get; set; }

        public IList<int> Adjacents { get; set; }
    }
}
