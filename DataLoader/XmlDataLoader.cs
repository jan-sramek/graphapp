﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml.Linq;

namespace DataLoader
{
    internal class XmlDataLoader
    {
        private string _folderPath;
        private List<NodeData> Nodes = new List<NodeData>();

        private XmlDataLoader(string folderPath)
        {
            _folderPath = folderPath;   
        }

        public static void Run(string folderPath)
        {
            var xmlDataLoader = new XmlDataLoader(folderPath);
            xmlDataLoader.ParseFiles();
            xmlDataLoader.InsertIntoDatabase();
        }

        private IList<string> GetAllXmlFiles()
        {
            return Directory.EnumerateFiles(_folderPath, "*.xml").ToList();            
        }

        private void ParseFiles()
        {
            foreach(var file in GetAllXmlFiles())
            {
                try
                {
                    var doc = XDocument.Load(file);
                    var nodes = doc.Elements("node")
                                  .Select(node => new NodeData
                                  {
                                      Id = (int)node.Element("id"),
                                      Label = (string)node.Element("label"),
                                      Adjacents = node.Element("adjacentNodes").Elements("id").Any() ? node.Element("adjacentNodes").Elements("id").Select(adj => Convert.ToInt32(adj.Value)).ToList() : new List<int>()
                                  })
                                  .ToList();

                    Nodes.AddRange(nodes);
                    Console.WriteLine($"Parsing of file {file} was successful.");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Parsing of file {file} failed.");
                    Console.WriteLine(e.ToString());
                }
            }           
        }

        private void InsertIntoDatabase()
        {
            foreach(var node in Nodes)
            {
                try
                {
                    var dataManagerClient = new DataManagerService.DataManagerClient();
                    dataManagerClient.InsertNode(node.Id, node.Label, node.Adjacents.ToArray());

                    Console.WriteLine($"Storing of node({node.Id}, {node.Label}) was successful.");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Storing of node({node.Id}, {node.Label}) failed.");
                    Console.WriteLine(e.ToString());
                }        
            }
        }
    }
}
